import { ServiceBroker } from "moleculer";

import AccountManagerService from "./services/manager";
import RedditTokenManagerService from "./services/reddit";

export default function registerAllAccountsServices(broker: ServiceBroker): void {
    broker.createService(AccountManagerService);
    broker.createService(RedditTokenManagerService);
}

export {
    AccountManagerService,
    RedditTokenManagerService
}