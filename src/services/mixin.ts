import { ServiceSchema } from "moleculer";
import Vault from "node-vault";
import delay from "delay";

module.exports = {
    name: "accounts.core",
    version: 4,
    settings: {
        $secureSettings: ["db", "vault"],
        db: {
            connectionString: process.env.DB_CONNECTION_STRING
        },
        vault: {
            apiVersion: process.env.ACCOUNTS_VAULT_VERSION || "v1",
            endpoint: process.env.ACCOUNTS_VAULT_ENDPOINT
        }
    },
    created() {
        // Throw error if Vault endpoint is not supplied
        if (!this.settings.vault.endpoint) {
            this.broker.fatal("Vault logger endpoint not supplied! Make sure ACCOUNTS_VAULT_ENDPOINT is populated!");
        }

        this.vault = Vault(this.settings.vault);
    },
    async started() {
        if (!(await this.vault.initialized()).initialized) {
            try {
                let results = await this.vault.init({secret_shares: 1, secret_threshold: 1});
                await this.vault.unseal({secret_shares: 1, key: results.keys[0]});
            } catch (err) {
                // In case of race condition with other services
                if (err.message !== "Vault is already initialized") {
                    throw err
                }
            }
        }

        while ((await this.vault.status()).sealed) {
            this.logger.info("Vault is sealed, sleeping for 1 second...");
            await delay(1000);
        }
    }
} as ServiceSchema