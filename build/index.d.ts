import { ServiceBroker } from "moleculer";
import AccountManagerService from "./services/manager";
import RedditTokenManagerService from "./services/reddit";
export default function registerAllAccountsServices(broker: ServiceBroker): void;
export { AccountManagerService, RedditTokenManagerService };
