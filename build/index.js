"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RedditTokenManagerService = exports.AccountManagerService = void 0;
const tslib_1 = require("tslib");
const manager_1 = tslib_1.__importDefault(require("./services/manager"));
exports.AccountManagerService = manager_1.default;
const reddit_1 = tslib_1.__importDefault(require("./services/reddit"));
exports.RedditTokenManagerService = reddit_1.default;
function registerAllAccountsServices(broker) {
    broker.createService(manager_1.default);
    broker.createService(reddit_1.default);
}
exports.default = registerAllAccountsServices;
