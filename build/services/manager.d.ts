import { Service, Context } from "moleculer";
export default class AccountManagerService extends Service {
    constructor(broker: any);
    getDefaultAccount(ctx: Context<{
        serviceType: SupportedServiceTypes;
    }, any>): Promise<{
        serviceType: any;
        userId: any;
        displayName: any;
        accessToken: any;
    }>;
    getDefaultAccessToken(ctx: Context<{
        serviceType: SupportedServiceTypes;
    }, any>): Promise<unknown>;
    getLinkedAccount(ctx: Context<{
        serviceType: SupportedServiceTypes;
        userId: string;
        requestedServiceType: SupportedServiceTypes;
    }, any>): Promise<{
        serviceType: any;
        userId: any;
        displayName: any;
        avatarUrl: any;
    }>;
}
declare type SupportedServiceTypes = "reddit";
export {};
