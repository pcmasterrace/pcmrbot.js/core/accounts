"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moleculer_1 = require("moleculer");
const AccountsCoreService = require("./mixin");
class AccountManagerService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: "accounts",
            version: 4,
            mixins: [AccountsCoreService],
            // TODO: Remove these once actual service is implemented
            actions: {
                getDefaultAccount: {
                    name: "default.get",
                    params: {
                        serviceType: {
                            type: "enum",
                            values: ["reddit"]
                        }
                    },
                    handler: this.getDefaultAccount
                },
                getDefaultAccessToken: {
                    name: "default.get.access",
                    params: {
                        serviceType: {
                            type: "enum",
                            values: ["reddit"]
                        }
                    },
                    handler: this.getDefaultAccessToken
                },
                getLinkedAccount: {
                    name: "linked.get",
                    params: {
                        serviceType: {
                            type: "enum",
                            values: ["reddit"]
                        },
                        userId: {
                            type: "string",
                            empty: false,
                            trim: true,
                            convert: true
                        },
                        requestedServiceType: {
                            type: "enum",
                            values: ["reddit"]
                        }
                    },
                    handler: this.getLinkedAccount
                }
            }
        });
    }
    async getDefaultAccount(ctx) {
        return {
            serviceType: undefined,
            userId: undefined,
            displayName: undefined,
            accessToken: undefined,
        };
    }
    async getDefaultAccessToken(ctx) {
        switch (ctx.params.serviceType) {
            case "reddit":
                return await ctx.call("v4.accounts.reddit.token.refresh", {
                    refreshToken: process.env.ACCOUNTS_REDDIT_OAUTH_REFRESHTOKEN
                });
            default:
                throw new moleculer_1.Errors.MoleculerClientError("Service type not supported! How did you even do this?", 404, "SERVICE_TYPE_NOT_SUPPORTED", ctx.params.serviceType);
        }
    }
    async getLinkedAccount(ctx) {
        return {
            serviceType: undefined,
            userId: undefined,
            displayName: undefined,
            avatarUrl: undefined
        };
    }
}
exports.default = AccountManagerService;
