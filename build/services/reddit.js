"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const moleculer_1 = require("moleculer");
const request_promise_native_1 = tslib_1.__importDefault(require("request-promise-native"));
const bottleneck_1 = tslib_1.__importDefault(require("bottleneck"));
const AccountsCoreService = require("./mixin");
class RedditTokenManagerService extends moleculer_1.Service {
    // public vault: any;
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: "accounts.reddit",
            version: 4,
            // mixins: [ AccountsCoreService ],
            settings: {
                $secureSettings: ["reddit.oauth.clientSecret"],
                bottleneck: {
                    redis: {
                        host: process.env.ACCOUNTS_REDIS_HOST,
                        port: Number(process.env.ACCOUNTS_REDIS_PORT) || 6379
                    }
                },
                reddit: {
                    oauth: {
                        userAgent: process.env.ACCOUNTS_REDDIT_OAUTH_USERAGENT,
                        clientId: process.env.ACCOUNTS_REDDIT_OAUTH_CLIENTID,
                        clientSecret: process.env.ACCOUNTS_REDDIT_OAUTH_CLIENTSECRET,
                        redirectUri: process.env.ACCOUNTS_REDDIT_OAUTH_REDIRECTURI
                    }
                }
            },
            actions: {
                refreshAccessToken: {
                    name: "token.refresh",
                    params: {
                        refreshToken: {
                            type: "string",
                            empty: false
                        }
                    },
                    cache: {
                        ttl: 3590
                    },
                    handler: this.refreshAccessToken
                }
            },
            events: {
                "v4.accounts.reddit.cache.clear": this.clearCache
            },
            created: this.serviceCreated,
            stopped: this.serviceStopped
        });
    }
    async refreshAccessToken(ctx) {
        // TODO: Add error handling
        // TODO: Also figure out what's up with the scheduler
        let response = await this.rateLimiter.schedule(() => request_promise_native_1.default({
            method: "POST",
            uri: "https://www.reddit.com/api/v1/access_token",
            headers: {
                "User-Agent": this.settings.reddit.oauth.userAgent
            },
            auth: {
                user: this.settings.reddit.oauth.clientId,
                password: this.settings.reddit.oauth.clientSecret
            },
            formData: {
                grant_type: "refresh_token",
                refresh_token: ctx.params.refreshToken
            },
            json: true
        }));
        return response.access_token;
    }
    async clearCache(payload) {
        this.broker.cacher.clean("v4.accounts.reddit.token.**");
    }
    serviceCreated() {
        // Terminate if any of the required settings aren't supplied
        if (!this.settings.bottleneck.redis.host)
            this.broker.fatal("Redis host not supplied! Make sure ACCOUNTS_REDIS_HOST is populated!");
        if (!this.settings.reddit.oauth.userAgent)
            this.broker.fatal("Reddit user agent not supplied! Make sure ACCOUNTS_REDDIT_OAUTH_USERAGENT is populated!");
        if (!this.settings.reddit.oauth.clientId)
            this.broker.fatal("Reddit client ID not supplied! Make sure ACCOUNTS_REDDIT_OAUTH_CLIENTID is populated");
        if (!this.settings.reddit.oauth.clientSecret)
            this.broker.fatal("Reddit client secret not supplied! Make sure ACCOUNTS_REDDIT_OAUTH_CLIENTSECRET is populated");
        if (!this.settings.reddit.oauth.redirectUri)
            this.broker.fatal("Reddit redirect URI not supplied! Make sure ACCOUNTS_REDDIT_OAUTH_REDIRECTURI is populated");
        this.rateLimiter = new bottleneck_1.default({
            id: `${this.broker.nodeID}/api.reddit`,
            // Reddit API limits
            reservoir: 60,
            reservoirRefreshAmount: 60,
            reservoirRefreshInterval: 60 * 1000,
            maxConcurrent: 1,
            minTime: 500,
            datastore: "ioredis",
            clearDatastore: true,
            clientOptions: {
                host: this.settings.bottleneck.redis.host,
                port: this.settings.bottleneck.redis.port
            }
        });
    }
    async serviceStopped() {
        await this.broker.cacher.clean("v4.accounts.reddit.token.refresh**");
        await this.rateLimiter.disconnect();
    }
}
exports.default = RedditTokenManagerService;
