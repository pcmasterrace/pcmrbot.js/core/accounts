import { Service, Context } from "moleculer";
import Bottleneck from "bottleneck";
export default class RedditTokenManagerService extends Service {
    rateLimiter: Bottleneck;
    constructor(broker: any);
    refreshAccessToken(ctx: Context<{
        refreshToken: string;
    }, any>): Promise<any>;
    clearCache(payload: any): Promise<void>;
    serviceCreated(): void;
    serviceStopped(): Promise<void>;
}
